import $ from 'jquery';
import moment from 'moment'
import Swal from 'sweetalert2';

//******************************************
//    Change this for ENVIRONMENT !!!!     *
var production:any = true;   //  *
//******************************************
// ENVIRONMENT |  VALUE
//------------------------
//  Production |  true
//------------------------
//  Staging    |  false
//------------------------
//  Localhost  |   0
//------------------------
//DO NOT MESS WITH THE ENVIRONMENT VARIABLES

const iAgreeUrl = production ? 'https://pristyntech.com/whatsapp/bots/GTCConsent/enterValueForGTCConsent' : 'http://13.232.200.161/wa/bots/GTCConsent/enterValueForGTCConsent';

export function saveData() {
  let data = {
    "appId": localStorage.getItem("appointment_id"),
    "retireBot": true
  }
  try {
    return $.ajax({
      url: `${iAgreeUrl}`,
      'processData': false,
      type: 'POST',
      // headers: {
      //   'Authorization': `Bearer ${token}`
      // },
      contentType: 'application/json',
      data: JSON.stringify(data)
    })
  } catch (err) {
    alert('error in code' + err);
  }
}