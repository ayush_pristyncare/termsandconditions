import React from 'react'
import './cssfile.css'
import { Button } from 'react-bootstrap';
import { redirectToLink } from '../../common';
import { globalVariables } from '../../globals';

function ThankYou(props) {

  let {
    name,
    dateOfSurgery,
    surgery,
    hospital
  } = props;

  function handleClose() {
    redirectToLink(globalVariables['companyLink']);
  }

  return (
      <div className="thank-you-container">
        <div className="cross-icon"
          onClick={handleClose}
        ></div>
        <img src="https://www.pristyncare.com/gtc-consent/success-icon.svg"
          alt="success-image"
          className="icon"
        />
        <h1 className="text-align-center thankYou">
          <span>Thank you</span>
          &nbsp;
          <span className="thankYou1">{ name.split(" ")[0] }</span>
        </h1>
        <p className="gray-color text-align-center subHead">
          For the consent for your surgery
        </p>
        <div className="thank-you-grid">
          <div className="grid-title gray-color">
            Name
          </div>
          <div className="grid-seperator gray-color">
            -
          </div>
          <div className="dataGrid">
            { name }
          </div>
          <div className="grid-title gray-color">
            Date & Time of Surgery
          </div>
          <div className="grid-seperator">
            -
          </div>
          <div className="dataGrid">
            { dateOfSurgery }
          </div>
          <div className="grid-title gray-color">
            Name of surgery
          </div>
          <div className="grid-seperator">
            -
          </div>
          <div className="dataGrid">
            { surgery }
          </div>
          <div className="grid-title gray-color">
            Hospital Name
          </div>
          <div className="grid-seperator">
            -
          </div>
          <div className="dataGrid">
            { hospital }
          </div>
        </div>
      </div>
  )
}

export default ThankYou
