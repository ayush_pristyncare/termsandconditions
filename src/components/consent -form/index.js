import React, { Component } from 'react'
import './cssfile.css'
import { Button } from 'react-bootstrap';
import { saveData } from '../../environment';
import Swal from 'sweetalert2';
export class ConsentFormComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      pop:true,
    };
    
}

init = () =>{

  if (window.location.href) {
    console.log("-----",window.location.href)
    var url = new URL(window.location.href);
    var id = url.searchParams.get("appointment_id");
    var index = url.searchParams.get("index");
    if(id){
      localStorage.setItem('appointment_id', id.trim());
      this.setState({pop:true})
    }else{
        Swal.fire({
          title: 'Failed',
          text: 'Appointment Id is missing in the URL',
          icon: 'error'
        });
        this.setState({pop:false})
    }
    if(index){
      localStorage.setItem('gtcIndex', index.trim());
    }
  }
window.addEventListener('beforeunload', this.handleUnload);
}

  componentDidMount(){
    localStorage.clear();
    this.init();
  }

 



popOpen=()=> {
  this.callApiToTrackEvents("t&c event","User Open T&C Popup", "action")
  this.setState({ pop: true })
}

iAgree=()=>{
  saveData().then(res=>{
    window.open('https://www.pristyncare.com/','_self')
  }
  )}
callApiToTrackEvents = (method, methoddata, type) => {
  let reqbody = {
    appId:localStorage.getItem('appointment_id'),
    index:localStorage.getItem("gtcIndex") ? localStorage.getItem("gtcIndex") : "1",
    value:{
      method:method,
      methoddata:methoddata,
      timestamp:new Date().toISOString(),
      type:type
    }
  }
  // sendGTCEvents(reqbody)
}

  render() {
    return (
      <>{this.state.pop &&
        <div className="mainBody" style={{ background: '#f4f9ff'}} >
                  <div className="popHead">
                  <img src="https://img.pristyncare.com/new_brand%2Felements%2Flogo.svg"
                    className="brand_logo"
                    alt="Pristyncare logo"/>
                    <br/><br/>
                    Terms & Conditions
                  </div>
                  <div className="content">
                  <div className="textBulet">
                      <ol>
                        <li>
                          The Patient shall be required and therefore agrees to provide the cash deposit and/or cheques and insurance details if applicable to the company prior to admission in hospital towards discharge of his/her liabilities including but not limited to the payment of surgery cost etc. The patient shall also provide any extra amount applicable in case of an extra night stay once confirmed by the doctor/hospital in advance. The patient shall pay any outstanding amount daily or clear the dues to the hospital at the time of discharge.
                        </li>
                        <li>
                          The patient agrees, undertakes, and consents that any and/or all information/details/reports of the patient may be shared with the hospital/Pristyn Care and its staff and all those whose assistance would be required for giving effect to the procedure/Pre-Authorization process contemplated herein and the patient shall have no issues/grievances or any claims arising out of such sharing.
                        </li>
                        <li>
                          The patient hereby also expressly consents to be shifted to a higher center and/or any other hospital, consultation with any senior consultant(doctor) mid-way into a procedure/surgery should there be any requirement owing to any critical issue that may arise during procedure/surgery at his/her own cost and consequence. The patient shall not dispute any additional cost owing to such additional procedures being performed if required for his/her treatment and shall pay all and/or any such additional amount.
                        </li>
                        <li>
                          The patient undertakes and agrees to disclose all the relevant information/details regarding his ailment, history etc. Any complication/eventuality owing to such undisclosed information/details leading to rejection of the claim by TPA/Insurance company, would neither make the hospital nor the company (Pristyn Care) and/or its doctors, employees, agents, staff etc. liable at any stage and such liability/responsibility of the claim payment shall always rest with the patient. The patient further undertakes not to claim anything under such a circumstance either from the hospital or from the company (Pristyn care) and/or its assigned doctor, employees, agents, staff etc.
                        </li>
                        <li>
                          The patient undertakes and agrees to ensure no damage is done by him/her or his/her attendants to the hospital infrastructure or property for any reason whatsoever during or after his stay in the said hospital either by himself or by any of his/her relatives, friends, acquaintances etc. Any such damage caused during the procedure or after the discharge would make the patient liable for recovery from him/her such amount as may be decided and communicated to the patient by the hospital, in writing.
                        </li>
                        <li>
                          The patient consents that he/she will be discharged only upon clearance of all dues of the hospital for the services performed as well as compensation, if any, towards any damage to the infrastructure/property to the satisfaction and approval of the Doctor of the Company (Pristyn Care) as well as from the hospital to be given in writing. The approval for discharge will be provided by the Doctor representing the company (Pristyn Care) and the clearance from the hospital shall be provided by the authorized representative of the hospital. The patient undertakes to guarantee encashment of cheques towards discharge of his/her liabilities upon being presented in the first instance only, failing which the company (Pristyn Care) reserves all its right to recover the amount from the patient through every possible legal means.
                        </li>
                        <li>
                          The patient shall be required to fill up a consent form prior to being admitted for any surgical procedure/healthcare services. It will be the duty of Pristyn care to explain the intricacies, risks and consequences involved if any, which may be probable during the surgical procedure. The patient shall not thereafter plead ignorance regarding any and/or all information.
                        </li>
                        <li>
                          The Patient consents that every detail pertaining to the procedure/surgery/healthcare services, expenses/cost, payment mechanism and consequence of non-payment, delay in payment has been duly informed and as such the patient has no objection with signing any and/or all document required to give effect to the procedure/surgery/healthcare services and payment of dues towards discharge of his/her liability. All such formality shall be completed prior to discharge. The patient shall not thereafter plead ignorance regarding any and/or all information.
                        </li>
                        <li>
                          The patient consents to the healthcare service being provided jointly by the company and Hospital under the instruction, supervision, and guidance of the company through its doctors. The patient further consents that the hospital is assigned to raise the Invoice to him/her with the reference of the company and its doctors and shall have no objection and shall not dispute on these grounds at any time.
                        </li>
                        <li>
                          The patient agrees and undertakes and consents that the cost of procedure/surgery/healthcare services explained to and agreed by him/her is only an estimated cost and may vary or increase depending upon the complexities if any as determined by the doctor treating the patient at a later stage. The total/actual cost of the procedure/surgery/healthcare services shall be intimated at the time of discharge and the patient expressly agrees to pay and not dispute the same. The procedure/surgery/healthcare services cost includes room and OT charges, anesthetist, diagnostics, drugs and consumables and other charges, among others. Additional cash payable, if any, over and above insurance has been explained and agreed upon by the patient.
                        </li>
                        <li>
                          The patient understands and agrees expressly that any insurance claim not approved by the insurer shall not be made attributable to the company (Pristyn Care) or the hospital at any point of time and he shall duly pay the final bill amount whether the insurance claim has been approved or rejected. Any rejection of the Insurance claim would not absolve the patient from his liability to pay back the bill amount due. The Patient shall be liable to pay the amount to Pristyn Care/Hospital upon reimbursement from the insurance Company immediately and without any delay. The patient further agrees to make all the payments in cash/DD/electronic transfer where he/she is not covered by any insurance or the claim has not been approved prior to discharge or at the time of re-imbursement rejection/Closure.
                        </li>
                        <li>
                          The patient acknowledges that the company is primarily responsible for any litigation with the patient.
                        </li>
                        <li>
                          The patient agrees and undertakes to resolve any issues/disputes either with the hospitals or with the doctors of the company pertaining to the procedure in an amicable manner with mutual agreement and on mutual terms.
                        </li>
                        <li>
                          Pre and Post hospitalization expenses shall not be part of insurance reimbursement claims for surgery done at any hospital. Those would be filed separately. Pre and post hospitalization expenses would also include any tests to be conducted prior to and after the surgical procedure.
                        </li>
                        <li>
                          All amounts due to the company (Pristyn Care) would be remitted only in the company’s official account details, as below.
                        </li>
                      </ol>
                      <div className='popBottom'>
                        A/c Name: GHV Advanced Care Pvt Ltd<br />
                        A/C No. - 50200033466575<br />
                        IFSC - HDFC0001202<br />
                      </div>
                      <ol start="16">
                      <li>
                       I hereby authorize GHV Advanced Care Pvt. Ltd. (Pristyn Care) to obtain a copy of my case file from the hospital after my discharge and follow-up with regard to payment from TPA/Insurance company on my behalf and also agree to provide all necessary support in case of any documentary requirement, necessary to process the claim.
                      </li>
                      <li>
                      The patient understands, declares and agrees expressly that any insurance claim not approved/part approved or rejected by the insurer for any reason whatsoever including but not limited to the patient&#39;s past medical/surgical history or owing to any terms and conditions of the policy shall not be attributed to the company (Pristyn Care) or the hospital at any point of time and he shall duly pay the final bill amount whether the insurance claim has been approved/part approved or rejected in total. Any rejection/deduction of the Insurance claim would not absolve the patient from his liability to pay back the total final bill amount due. The Patient shall be liable to pay the amount to Pristyn Care/Hospital upon reimbursement from the insurance Company immediately and without any delay.
                      </li>
                      </ol>
                      <div id="tc-footer">
                      Upon agreeing to and signing of these general terms and conditions, the Patient provides its express consent and its confirmation for the Procedure/surgery/healthcare services on the said terms and conditions.
                      </div>
                    </div>
                  </div>
                  <div className="popBottom1">
                      <div className="iAgreePop">
                      <Button onClick={(e)=>{this.iAgree()}} className="pdfDownload pristynButton">I Agree</Button>
                      </div>
                  </div>
                </div>}</>
    )
  }
}

export default ConsentFormComponent;
