import React, { useState, useRef } from 'react'
import './cssfile.css'
import { redirectToLink } from '../../common';
import { globalVariables } from '../../globals';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faLightbulb } from '@fortawesome/free-solid-svg-icons'
import { Col, Form, Card, Button, Row, Modal, Image, InputGroup, ListGroup, ListItem, FormControl } from 'react-bootstrap';
import { generateOTP, verifyOTP, timeSubmission } from '../../environment';
import Swal from 'sweetalert2';
import { event } from 'jquery';

function FinalPop(props) {

  const otpFocus1 = useRef(null);
  const otpFocus2 = useRef(null);
  const otpFocus3 = useRef(null);

  const [otp, setOtp] = useState(false);

  const [otp0, setOtp0] = useState(false);
  const [otp1, setOtp1] = useState(false);
  const [otp2, setOtp2] = useState(false);
  const [otp3, setOtp3] = useState(false);
  const [otpVal, setOtpVal] = useState(['','','','']);
  const [cnf, setCnf] = useState(false);

  // this.handleClick = this.handleClick.bind(this);
  // const onCancel = (data) => {
  //   setValue(data)
  //   console.log("Form>", data);
  // }
  // const onValid = (data) => {
  //   setValue(data)
  //   console.log("Form>", data);
  // }

  let {
    phone
  } = props;

  // const onValid = () => {

  // }
  function handleClose() {
    // redirectToLink(globalVariables['companyLink']);
    // onCancel();
    props.onCancel();
  }
  function getOtp() {
    props.callApiToTrackEvents("otp request event", "User Requested OTP on "+phone, "otp")
    generateOTP(phone).then(res => {
      props.callApiToTrackEvents("otp sent event", "OTP Sent Successfully on "+phone, "otp")
      submitSwal('OTP Sent');
      setOtp(true);
    })
  }

  function validSwal(evt){
    Swal.fire({
      title: '',
      text: evt?evt:'',
      timer: 2000,
      icon: 'error',
      toast: true,
      showConfirmButton:false
    })
  }
  function submitSwal(evt){
    Swal.fire({
      title: '',
      text: evt,
      timer: 2000,
      icon: 'success',
      toast: true,
      showConfirmButton:false
    })
  }
  function fillOtp(evt,x){
    // let val = otpVal;
    // console.log(x)
    // console.log(evt.target.value[evt.target.value.length-1])
    // val[x]=evt.target.value[evt.target.value.length-1];
    // otpVal[x]=evt.target.value[evt.target.value.length-1];
    setOtpVal(otp0+otp1+otp2+otp3);
    switch (x) {
      case 0: otpFocus1.current.focus();
              setOtp0(evt.target.value[evt.target.value.length-1]);
              break;
      case 1: otpFocus2.current.focus();
              setOtp1(evt.target.value[evt.target.value.length-1]);
              break;
      case 2: otpFocus3.current.focus();
              setOtp2(evt.target.value[evt.target.value.length-1]);
              break;
      case 3: setOtp3(evt.target.value[evt.target.value.length-1]);
              setCnf(true);
              setOtpVal(otp0+otp1+otp2+evt.target.value[evt.target.value.length-1]);
              break;
      default:
        break;
    }

  }
  function confirmOtp(){
    if(cnf){
      // props.onValid()
      // let otpTmp = otpVal.reduce((a,i) => {
      //  return  a + i;
      // });
      // console.log("otpval",otpVal, otp)
      // submitSwal('Verified');
      //     setOtp(true);
      //     return
      props.callApiToTrackEvents("otp validate event", "User Enter Attendant OTP "+otpVal+" on "+phone, "otp")
      verifyOTP(phone,otpVal).then(res => {
        props.callApiToTrackEvents("submitted time event", "Consent Form Submitted Time "+new Date().toISOString(), "time")
        timeSubmission();
        if(res && res.status===200){
          props.callApiToTrackEvents("otp validate event", "OTP Validate Successfully on "+phone, "otp")
          submitSwal('Verified');
          setOtp(true);
          props.onValid()
        }else{
          this.callApiToTrackEvents("otp validate event", "OTP Not Validated on "+phone, "otp")
          validSwal('');
        }
      })

    }
  }
  function resendOtp(){
    generateOTP(phone).then(res => {
      // console.log(res)
      submitSwal('OTP Sent');
      setOtp(true);
    })
  }

  return (
      <div className="thank-you-container2">
        <div className='headPop'>
        <FontAwesomeIcon className="popIcon99" icon={faArrowLeft} onClick={handleClose} />
        <div className="cross-icon"
          onClick={handleClose}
        ></div>
        <div className="headPop1">
        Consent Submission
        </div>
        </div>
        <div className="infoPop">
        <FontAwesomeIcon className="popIcon1" icon={faLightbulb} /> &nbsp;Info<br/>
        <p>
          You will recieve an OTP on your registered number as you click on<br/>
          GET OTP
        </p>
        </div>
        <div className='mobPop'>
          Mobile Number
        </div>
        <div className='mobPop1'>
          { phone }
        </div>
        <div >
        <Row className="otpPop" hidden={!otp}>
        <Col xs={12}>
                <div className="form-body">
                  <label className="otp">Enter OTP</label>
                </div>
            </Col>
            <Col xs={2}>
              <input className="w3-input1" type="number" name='otp0' length="1" value={otp0} onChange={(evt) =>fillOtp(evt,0)}/>
            </Col>
            <Col xs={2}>
              <input className="w3-input1" type="number" name='otp1' length="1" ref={otpFocus1} value={otp1} onChange={(evt) =>fillOtp(evt,1)} />
            </Col>
            <Col xs={2}>
              <input className="w3-input1" type="number" name='otp2' length="1" ref={otpFocus2} value={otp2} onChange={(evt) =>fillOtp(evt,2)}/>
            </Col>
            <Col xs={2}>
              <input className="w3-input1" type="number" name='otp3' length="1" ref={otpFocus3} value={otp3} onChange={(evt) =>fillOtp(evt,3)} />
            </Col>
            <Col xs={4}>
            {/* <button type="button"  className="btn btn-primary noButton">CONFIRM</button> */}
            <a className='resend' onClick={resendOtp}>Resend</a>
            </Col>
            </Row>
        </div>
        <div hidden={otp} className="getPop" onClick={getOtp}>
          GET OTP
        </div>
        <div style={{background: cnf?'#3D7CC9':'grey'}} hidden={!otp} className="getPop" onClick={confirmOtp}>
          CONFIRM
        </div>
      </div>
  )
}

export default FinalPop
