import React , { useState } from 'react';
import logo from './logo.svg';
import './App.css';
// import useState from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
// import KycConsentComponent from './components/kyc-consent';
import ConsentFormComponent from './components/consent -form';
import NotFound from './components/not-found';

function App() {
  const [state, setstate] = useState({arrow:false})
  const changeState = () => {  
    setstate({arrow:`state/props of parent component 
    is send by onClick event to another component`}); 
   };

  return (
    <div className="App">
      <Router>
        <div className="appMain">
        <Switch>
          <Route exact path="/" component={ConsentFormComponent} />
          <Route component={NotFound} />
        </Switch>
        </div>
      </Router>
    </div>

  );
}

export default App;
