export function redirectToLink(link = 'pristyncare.com', nextTab = false) {
  let a = document.createElement('a');

  a.href = link;
  if(nextTab) {
    a.target = '_blank';
  }
  
  a.click();
}

export async function storeObjToLocalStorage(obj = {}) {
  try{
    Object.keys(obj).map( key => {
      localStorage.setItem(key, obj[key])
    })
    return "Keys Updated!";
  }
  catch(error){

  }

}